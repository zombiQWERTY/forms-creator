import { Dispatch, FC, SetStateAction } from 'react';
import { FormConfig } from '../Configurator';
import { GenericForm } from '../GenericForm';
import styles from './styles.module.css';

interface PreviewProps {
  onChange: Dispatch<SetStateAction<FormConfig>>;
  formConfig: FormConfig;
}

export const Preview: FC<PreviewProps> = ({ formConfig, onChange }) => {
  return (
    <>
      <div className={styles.wrapper}>
        <div className={styles.content}>
          <GenericForm config={formConfig} onChange={onChange} isExport={false} />
        </div>
      </div>
    </>
  );
};
