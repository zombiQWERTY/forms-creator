import { assoc, identity, trim } from 'ramda';
import { renderToStaticMarkup } from 'react-dom/server';
import { Dispatch, FC, SetStateAction, useEffect, useState } from 'react';
import { InputButton } from '../../components/formComponents/inputButton';
import { InputText } from '../../components/formComponents/inputText';
import { InputTextarea } from '../../components/formComponents/inputTextarea';
import { Modal } from '../../components/Modal';
import { Field } from '../GenericForm/types';
import styles from './styles.module.css';
import { GenericForm } from '../GenericForm';

export interface FormConfig {
  title: string;
  service: boolean;
  fields: Field[];
  style: string;
}

interface ConfiguratorProps {
  onChange: Dispatch<SetStateAction<FormConfig>>;
  formConfig: FormConfig;
}

export const Configurator: FC<ConfiguratorProps> = ({ onChange, formConfig }) => {
  const [title, setTitle] = useState('Base Form');
  const [showModal, setShowModal] = useState(false);

  const handleSubmit = (data: FormConfig) => {
    onChange({ ...data, service: true });
  };

  useEffect(() => {
    handleSubmit({ ...formConfig, title, service: true });
  }, [title]);

  return (
    <>
      <div className={styles.wrapper}>
        <div className={styles.inlineInputs}>
          <InputText name="title" label="Title" value={title} onChange={setTitle} />
          <InputButton label="Export Form" onClick={() => setShowModal(true)} />
        </div>
      </div>
      {showModal && (
        <Modal setShow={setShowModal} show={showModal}>
          <InputTextarea
            label="Your HTML"
            name="html"
            value={renderToStaticMarkup(
              <GenericForm config={assoc('service', false, formConfig)} isExport={true} onChange={identity} />,
            )}
            onChange={identity}
          />
          <InputTextarea label="Your CSS" name="css" value={unescape(trim(formConfig.style))} onChange={identity} />
        </Modal>
      )}
    </>
  );
};
