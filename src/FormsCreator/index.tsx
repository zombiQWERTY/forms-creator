import { useState } from 'react';
import { Configurator, FormConfig } from './Configurator';
import { Preview } from './Preview';
import styles from './root.module.css';

export const FormsCreator = () => {
  const [formConfig, setFormConfig] = useState<FormConfig>({
    title: 'Base Form',
    service: true,
    fields: [],
    style: '',
  });

  return (
    <>
      <div className={styles.wrapper}>
        <div className={styles.title}>
          <h1>Forms Creator</h1>
        </div>
        <div className={styles.container}>
          <div className={styles.configurator}>
            <Configurator onChange={setFormConfig} formConfig={formConfig} />
          </div>
          <div className={styles.preview}>
            <Preview formConfig={formConfig} onChange={setFormConfig} />
          </div>
        </div>
      </div>
    </>
  );
};
