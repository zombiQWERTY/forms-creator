export type Type = 'text' | 'select' | 'checkbox' | 'button';
export type InputType = 'text' | 'email' | 'phone' | 'number';
export type Options = Array<{ label: string; value: string }>;

export interface Field {
  type: Type;
  label: string;
  name?: string;
  required?: boolean;
  params?: {
    inputType?: InputType;
    placeholder?: string;
    options?: Options;
  };
}
