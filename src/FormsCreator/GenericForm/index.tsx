import { append, last, remove, update } from 'ramda';
import { Dispatch, FC, SetStateAction, useEffect, useState } from 'react';
import { Tooltip } from '../../components/Tooltip';
import { FormConfig } from '../Configurator';
import { ElementForm } from './innerForms/ElementForm';
import styles from './styles.module.css';
import { Field } from './types';

const style = `
.fc-form-root {
  line-height: 1;
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}

.fc-form-root,
.fc-form-root div,
.fc-form-root span,
.fc-form-root h1,
.fc-form-root h2,
.fc-form-root h3,
.fc-form-root h4,
.fc-form-root h5,
.fc-form-root h6,
.fc-form-root p,
.fc-form-root a,
.fc-form-root img,
.fc-form-root form,
.fc-form-root label,
.fc-form-root input,
.fc-form-root select {
  margin: 0;
  padding: 0;
  border: 0;
  font-size: 100%;
  vertical-align: baseline;
  box-sizing: border-box;
}

.fc-form-root h1,
.fc-form-root h2,
.fc-form-root h3,
.fc-form-root h4,
.fc-form-root h5,
.fc-form-root h6 {
  font-weight: 400;
}

.fc-form-root {
  width: 600px;
  margin: 0 auto;
  border: 2px solid #f7f7f9;
  padding: 20px;
  background: #fff;
  box-shadow: 0 0 5px 0px rgba(34, 60, 80, 0.2);
}

.fc-form-root .fc-title {
  text-align: center;
  margin-bottom: 30px;
  font-size: 20px;
}

.fc-form-root .fc-label {
  margin-bottom: 20px;
  display: block;
  position: relative;
}

.fc-form-root .fc-label-title {
  margin-bottom: 10px;
  position: relative;
  display: inline-block;
}

.fc-form-root .fc-input {
  width: 100%;
  border: 1px solid #ced4da;
  border-radius: 5px;
  padding: 10px 15px;
  font-size: 16px;
}

.fc-form-root .fc-input-required::before {
  content: '*';
  color: #ff0000;
  position: absolute;
  z-index: 100;
  margin-left: 2px;
}

.fc-form-root .fc-checkbox-label {
  display: block;
  user-select: none;
  padding: 5px 0;
}

.fc-form-root .fc-input-button {
  cursor: pointer;
  background: #008793;
  display: inline-block;
  border: none;
  padding: 10px 15px;
  color: #fff;
  user-select: none;
}
`;

interface GenericFormProps {
  onChange: Dispatch<SetStateAction<FormConfig>>;
  config: FormConfig;
  isExport: boolean;
}

export const GenericForm: FC<GenericFormProps> = ({ config, onChange, isExport }) => {
  const [fields, setFields] = useState<Field[]>([
    {
      type: 'text',
      name: 'first_name',
      label: 'First Name',
      required: true,
      params: {
        inputType: 'text',
        placeholder: 'Enter First Name',
      },
    },
    {
      type: 'text',
      name: 'email',
      label: 'Email',
      required: true,
      params: {
        inputType: 'email',
        placeholder: 'Enter Email',
      },
    },
    {
      type: 'button',
      label: 'Submit',
    },
  ]);

  useEffect(() => {
    onChange({ ...config, fields, style });
  }, [fields]);

  const handleAdd = (f: Field | null) => setFields(append(f as Field, fields));
  const handleEdit = (idx: number) => (f: Field | null) =>
    setFields(f ? update(idx, f, fields) : remove(idx, 1, fields));

  const FormTag = config.service ? 'div' : 'form';

  return (
    <>
      {!isExport && <style>{style}</style>}
      <div className="fc-form-root">
        <FormTag>
          <div>
            <h1 className="fc-title">{config.title}</h1>
            {fields.map((f, idx) => {
              switch (f.type) {
                case 'text': {
                  return (
                    <div key={idx} className="fc-label">
                      {config.service && (
                        <div className={styles.inputSettingsWrapper}>
                          <Tooltip
                            content={(close: () => void) => (
                              <ElementForm onSubmit={handleEdit(idx)} fieldData={f} closeParent={close} />
                            )}>
                            <div className={styles.inputSettings}>Settings</div>
                          </Tooltip>
                        </div>
                      )}
                      <label>
                        <span className="fc-label-title">
                          {f.label}
                          {f.required && <span className="fc-input-required"></span>}
                        </span>
                        <input
                          className="fc-input"
                          type={f?.params?.inputType}
                          placeholder={f?.params?.placeholder}
                          required={f.required}
                          name={f.name}
                        />
                      </label>
                    </div>
                  );
                }
                case 'select': {
                  return (
                    <div key={idx} className="fc-label">
                      {config.service && (
                        <div className={styles.inputSettingsWrapper}>
                          <Tooltip
                            content={(close: () => void) => (
                              <ElementForm onSubmit={handleEdit(idx)} fieldData={f} closeParent={close} />
                            )}>
                            <div className={styles.inputSettings}>Settings</div>
                          </Tooltip>
                        </div>
                      )}
                      <label>
                        <span className="fc-label-title">
                          {f.label}
                          {f.required && <span className="fc-input-required"></span>}
                        </span>
                        <select required={f.required} name={f.name} className="fc-input">
                          {f?.params?.options?.map((opt, idx) => {
                            return (
                              <option key={idx} value={opt.value}>
                                {opt.label}
                              </option>
                            );
                          })}
                        </select>
                      </label>
                    </div>
                  );
                }
                case 'checkbox': {
                  return (
                    <div key={idx} className="fc-label">
                      {config.service && (
                        <div className={styles.inputSettingsWrapper}>
                          <Tooltip
                            content={(close: () => void) => (
                              <ElementForm onSubmit={handleEdit(idx)} fieldData={f} closeParent={close} />
                            )}>
                            <div className={styles.inputSettings}>Settings</div>
                          </Tooltip>
                        </div>
                      )}
                      <span className="fc-label-title">
                        {f.label}
                        {f.required && <span className="fc-input-required"></span>}
                      </span>
                      <div>
                        {f?.params?.options?.map((opt, idx) => {
                          return (
                            <label className="fc-checkbox-label">
                              <input key={idx} type="checkbox" required={f.required} name={f.name} value={opt.value} />{' '}
                              {opt.label}
                            </label>
                          );
                        })}
                      </div>
                    </div>
                  );
                }
                case 'button': {
                  return (
                    <div key={idx} className="fc-label">
                      <input className="fc-input-button" type="submit" value={f.label} />
                      {config.service && (
                        <div className={styles.inputSettingsWrapper}>
                          <Tooltip
                            content={(close: () => void) => (
                              <ElementForm onSubmit={handleEdit(idx)} fieldData={f} closeParent={close} />
                            )}>
                            <div className={styles.inputSettings}>Settings</div>
                          </Tooltip>
                        </div>
                      )}
                    </div>
                  );
                }
                default:
                  <></>;
              }
            })}
            {config.service && last(fields)?.type !== 'button' && (
              <Tooltip content={(close: () => void) => <ElementForm onSubmit={handleAdd} closeParent={close} />}>
                <div className={styles.inputAddField}>Add field</div>
              </Tooltip>
            )}
          </div>
        </FormTag>
      </div>
    </>
  );
};
