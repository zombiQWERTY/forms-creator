import { filter, join, map, pluck, split, trim } from 'ramda';
import { FC, useState } from 'react';
import { Form } from '../../../components/formComponents/form';
import { InputButton } from '../../../components/formComponents/inputButton';
import { InputSelect } from '../../../components/formComponents/inputSelect';
import { InputSubmit } from '../../../components/formComponents/inputSubmit';
import { InputText } from '../../../components/formComponents/inputText';
import { Field, Type } from '../types';
import styles from './styles.module.css';

interface ElementFormProps {
  onSubmit: (f: Field | null) => void;
  closeParent: () => void;
  fieldData?: Field;
}

export const ElementForm: FC<ElementFormProps> = ({ onSubmit, closeParent, fieldData }) => {
  const [type, setType] = useState(fieldData?.type || 'text');
  const [name, setName] = useState(fieldData?.name || '');
  const [label, setLabel] = useState(fieldData?.label || '');
  const [required, setRequired] = useState(Boolean(fieldData?.required));
  const [inputType, setInputType] = useState(fieldData?.params?.inputType || 'text');
  const [placeholder, setPlaceholder] = useState(fieldData?.params?.placeholder || '');
  const [options, setOptions] = useState(fieldData?.params?.options || []);

  const onTypeChange = (v: Type) => {
    setType(v);
    setInputType('text');
    setPlaceholder('');
    return v;
  };

  const onRequiredChange = (v?: string) => {
    if (v) {
      const res = v === 'required';
      setRequired(res);
      return res;
    }
  };

  const onOptionsChange = (v?: string) => {
    if (v) {
      const res = map((o: string) => {
        const opt = trim(o);
        return { value: opt, label: opt };
      }, split(',', v));

      setOptions(res);
      return res;
    }
  };

  const handleSubmit = (data: any) => {
    onSubmit({
      type: onTypeChange(data.type),
      name: data.name,
      label: data.label,
      required: onRequiredChange(data.required),
      params: {
        inputType: data.inputType,
        placeholder: data.placeholder,
        options: onOptionsChange(data.options),
      },
    });
    closeParent();
  };

  return (
    <>
      <Form onSubmit={handleSubmit} className={styles.tooltipForm}>
        <div className={styles.formItem}>
          <InputSelect
            name="type"
            label="Field Type"
            options={filter(
              (v) => (fieldData ? v.value !== 'button' : true),
              [
                { value: 'text', label: 'Text' },
                { value: 'select', label: 'Select' },
                { value: 'checkbox', label: 'Checkbox' },
                { value: 'button', label: 'Submit Button' },
              ],
            )}
            value={type}
            onChange={onTypeChange}
          />
        </div>
        {type !== 'button' && (
          <div className={styles.formItem}>
            <InputText name="name" label="Name" value={name} onChange={setName} />
          </div>
        )}
        <div className={styles.formItem}>
          <InputText name="label" label="Label" value={label} onChange={setLabel} />
        </div>
        {type !== 'button' && (
          <div className={styles.formItem}>
            <InputSelect
              name="required"
              label="Required?"
              options={[
                { value: 'required', label: 'Required' },
                { value: 'not-required', label: 'Not Required' },
              ]}
              value={required ? 'required' : 'not-required'}
              onChange={onRequiredChange}
            />
          </div>
        )}
        {type === 'text' && (
          <>
            <div className={styles.formItem}>
              <InputSelect
                name="inputType"
                label="Text Input Type"
                options={[
                  { value: 'text', label: 'Text' },
                  { value: 'email', label: 'Email' },
                  { value: 'phone', label: 'Phone' },
                  { value: 'number', label: 'Number' },
                ]}
                value={inputType}
                onChange={setInputType}
              />
            </div>
            <div className={styles.formItem}>
              <InputText name="placeholder" label="Placeholder" value={placeholder} onChange={setPlaceholder} />
            </div>
          </>
        )}
        {(type === 'select' || type === 'checkbox') && (
          <div className={styles.formItem}>
            <InputText
              name="options"
              label="Options (comma separated)"
              value={join(',', pluck('value', options))}
              onChange={onOptionsChange}
            />
          </div>
        )}
        <div className={styles.formItem}>
          <InputSubmit label={fieldData ? 'Edit Field' : 'Add Field'} />
          {fieldData && <InputButton danger label="Remove" onClick={() => onSubmit(null)} />}
        </div>
      </Form>
    </>
  );
};
