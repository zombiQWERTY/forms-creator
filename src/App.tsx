import React from 'react';
import './App.css';
import { FormsCreator } from './FormsCreator';

function App() {
  return (
    <div className="app">
      <div className="content">
        <FormsCreator />
      </div>
    </div>
  );
}

export default App;
