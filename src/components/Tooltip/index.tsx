import { FC, ReactNode, useState } from 'react';
import styles from './styles.module.css';

interface TooltipProps {
  children: ReactNode;
  content: (f: () => void) => ReactNode;
}

export const Tooltip: FC<TooltipProps> = ({ children, content }) => {
  const [show, setShow] = useState(false);

  return (
    <div className={styles.wrapper}>
      <div onClick={() => setShow(!show)}>{children}</div>
      {show && <div className={styles.content}>{content(() => setShow(false))}</div>}
    </div>
  );
};
