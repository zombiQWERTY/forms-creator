import { Dispatch, FC, SetStateAction } from 'react';
import styles from './styles.module.css';

interface Option {
  label: string;
  value: string;
}

interface InputChoiceProps {
  label: string;
  name: string;
  value: string;
  multi?: boolean;
  options: Option[];
  onChange: Dispatch<SetStateAction<string>> | ((v: string) => void);
}

export const InputChoice: FC<InputChoiceProps> = ({ onChange, name, value, label, multi = false, options }) => {
  const type = multi ? 'checkbox' : 'radio';

  const onInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    onChange(e.currentTarget.value);
  };

  return (
    <div>
      <div className={styles.title}>{label}</div>
      {options.map((opt, idx) => {
        return (
          <label className={styles.inputInlineWrapper} key={idx}>
            <input type={type} name={name} value={opt.value} onChange={onInputChange} checked={value === opt.value} />
            {opt.label}
          </label>
        );
      })}
    </div>
  );
};
