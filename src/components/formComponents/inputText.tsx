import { Dispatch, FC, SetStateAction } from 'react';
import styles from './styles.module.css';

interface InputTextProps {
  label: string;
  name: string;
  value: string;
  onChange: Dispatch<SetStateAction<string>> | ((v: string) => void);
}

export const InputText: FC<InputTextProps> = ({ onChange, name, value, label }) => {
  const onInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    onChange(e.currentTarget.value);
  };

  return (
    <div>
      <label className={styles.inputDefaultWrapper}>
        <div className={styles.title}>{label}</div>
        <input className={styles.input} type="text" name={name} value={value} onChange={onInputChange} />
      </label>
    </div>
  );
};
