import { Dispatch, FC, SetStateAction } from 'react';
import styles from './styles.module.css';

interface Option {
  label: string;
  value: string;
}

interface InputSelectProps {
  label: string;
  name: string;
  value: string;
  options: Option[];
  onChange: Dispatch<SetStateAction<any>> | ((v: any) => void);
}

export const InputSelect: FC<InputSelectProps> = ({ onChange, name, value, label, options }) => {
  const onInputChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    onChange(e.currentTarget.value);
  };

  return (
    <div>
      <label>
        <div className={styles.title}>{label}</div>
        <select name={name} onChange={onInputChange} className={styles.input} value={value}>
          {options.map((opt, idx) => {
            return (
              <option key={idx} value={opt.value}>
                {opt.label}
              </option>
            );
          })}
        </select>
      </label>
    </div>
  );
};
