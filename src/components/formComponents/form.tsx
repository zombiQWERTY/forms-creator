import React, { Dispatch, FC, ReactNode, SetStateAction } from 'react';

interface FormProps {
  children: ReactNode;
  className?: string;
  onSubmit: Dispatch<SetStateAction<any>>;
}

export const Form: FC<FormProps> = ({ onSubmit, children, className }) => {
  const handleSubmit = (e: React.ChangeEvent<HTMLFormElement>) => {
    e.preventDefault();
    const data = new FormData(e.target);
    onSubmit(Object.fromEntries(data));
  };

  return (
    <form onSubmit={handleSubmit} className={className}>
      {children}
    </form>
  );
};
