import { FC } from 'react';
import styles from './styles.module.css';

interface InputSubmitProps {
  label: string;
}

export const InputSubmit: FC<InputSubmitProps> = ({ label }) => {
  return (
    <label className={styles.inputButtonWrapper}>
      <div>{label}</div>
      <input className={styles.inputButton} type="submit" value={label} />
    </label>
  );
};
