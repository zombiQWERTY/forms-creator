import { FC } from 'react';
import cm from 'classnames';
import styles from './styles.module.css';

interface InputButtonProps {
  label: string;
  danger?: boolean;
  onClick: () => void;
}

export const InputButton: FC<InputButtonProps> = ({ label, danger, onClick }) => {
  return (
    <div className={cm(styles.inputButtonWrapper, { [styles.danger]: danger })} onClick={onClick}>
      <div>{label}</div>
      <input className={styles.inputButton} type="button" value={label} />
    </div>
  );
};
