import { Dispatch, FC, SetStateAction } from 'react';
import styles from './styles.module.css';

interface InputTextareaProps {
  label: string;
  name: string;
  value: string;
  onChange: Dispatch<SetStateAction<string>> | ((v: string) => void);
}

export const InputTextarea: FC<InputTextareaProps> = ({ onChange, name, value, label }) => {
  const onInputChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    onChange(e.currentTarget.value);
  };

  return (
    <div>
      <label className={styles.inputDefaultWrapper}>
        <div className={styles.title}>{label}</div>
        <textarea className={styles.inputTextarea} name={name} value={value} onChange={onInputChange} />
      </label>
    </div>
  );
};
