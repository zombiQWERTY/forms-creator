import { Dispatch, FC, ReactNode, SetStateAction } from 'react';
import { InputButton } from '../formComponents/inputButton';
import styles from './styles.module.css';

interface ModalProps {
  children: ReactNode;
  setShow: Dispatch<SetStateAction<boolean>> | ((v: boolean) => void);
  show: boolean;
}

export const Modal: FC<ModalProps> = ({ children, show, setShow }) => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.content}>
        <div className={styles.close}>
          <InputButton label="Close" onClick={() => setShow(false)} />
        </div>
        {children}
      </div>
    </div>
  );
};
